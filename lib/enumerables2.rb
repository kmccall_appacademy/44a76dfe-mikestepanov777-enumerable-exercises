require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.inject(0, :+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all?{|str| str.include?(substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string.gsub(/\s/, "").chars.select{|chr| string.count(chr) > 1}.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.scan(/\w+/).sort_by{|str| str.length}[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").select{|chr| chr unless string.include?(chr)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select{|y| y if not_repeat_year?(y)}
end

def not_repeat_year?(year)
  digits = year.to_s.chars
  digits == digits.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.select{|song| song if no_repeats?(song, songs)}.uniq
end

def no_repeats?(song_name, songs)
  arr = songs.map.with_index{|s, idx| idx if s == song_name}.compact
  (0...arr.size - 1).each do |idx|
    return false if arr[idx] == arr[idx + 1] - 1
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = string.scan(/\w+/).select{|str| str.count("c") > 0}
  words.min_by{|str| c_distance(str)}
end

def c_distance(word)
  word.reverse.index("c")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  squeeze = squeeze(arr.dup)
  return_arr = []
  idx = 0
  squeeze.each do |chr|
    start_idx = idx
    repeats = 0
    while arr[idx] == chr
      idx += 1
      repeats += 1
    end
    next if repeats < 2
    end_idx = start_idx + repeats - 1
    return_arr << [start_idx, end_idx]
  end
  return_arr
end

def squeeze(arr)
  arr.map.with_index{|chr, idx| chr if arr[idx + 1] != chr}.compact
end
